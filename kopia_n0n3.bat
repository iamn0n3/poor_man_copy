@echo off
rem
rem by Adrian Kubok
rem
rem 20140730 - dodano kasowanie kopii "starego typu"
rem 20140730 - wyszukiwanie zmiennych, ujednolicenie plikow
rem 20140726 - rozbudowa
rem 20140725 - utworzenie pliku, migracja z pojedynczego skryptu na wieloplikowy
rem 201502xx - przy przywracaniu nadpisuj nowsze pliki!
rem 20150516 - wylaczenie nieuzywanej kopii JM STUDIO w malej ksiegowosci
rem 20150516 - ustawiono czekanie 1s po kazdej kopii
rem 20160111 - poprawki w nazwach logow, dodanie kopii ustawien office
rem 20160118 - poprawki w kopii office, dodano zmianna CZEKAJ
rem 20160224 - dodano czyszczenie folderow
rem

rem ############### Parametry do zmiany #######################################

SET VER=kopia.001

SET CZEKAJ=1

rem ustaw miejsce utworzenia logu
SET LOG="U:\autokopia\log"

rem ustaw plik potwierdzajacy ukonczenie kopii
SET LATEST="U:\autokopia\latest.000"

rem ustaw plik potwierdzajacy ukonczenie kopii
SET LATEST2="U:\autokopia\kopia.001"

rem ustaw plik kopii
SET LOGFILE="%LOG%\kopia_%DATE%.log"

rem ustaw log robocopy
SET LOGROBO="%LOG%\robocopyK_%DATE%.log"

rem ustaw miejsce kopii logu
rem SET LOGSRV="\\medrek.admit.net.pl\LOG$\%username%\"

rem ustaw spis kopii
SET LOGLOG="\\admit.net.pl\ADMIT\LOG\kopie_log.txt"

rem ustaw miejsce utworzenia kopii
SET KOPIA="U:\autokopia"

rem gdzie jest robocopy
SET ROBO="T:\install\autokopia\robocopy.exe"

rem ustaw opcje robocopy
SET PARMS=/MIR /COPY:DAT /R:10 /W:3 /NP /V /LOG+:%LOGROBO% /XO /XD Cache

rem kierunek kopiowania
SET KIERUNEK=KOPIA
IF "%1"=="PRZYWROC" SET KIERUNEK=PRZYWROC
IF "%1"=="PRZYWROC" SET PARMS=/MIR /COPY:DAT /R:10 /W:1 /NP /V /LOG+:%LOGROBO% /XD Cache /XD $RECYCLE.BIN
IF "%1"=="PRZYWROC" SET LOGFILE="%LOG%\przywroc_%DATE%.txt"
IF "%1"=="PRZYWROC" SET LOGROBO="%LOG%\robocopyP_%DATE%.txt"


rem ############### Sprawdz zmienne ###########################################
echo %DATE% %TIME% > %LOGFILE%
IF EXIST %LOGROBO% del %LOGROBO%
rem IF NOT EXIST %LOGSRV% mkdir %LOGSRV%
rem jezeli kopia jest starego formatu to skasuj
IF NOT EXIST %KOPIA%\%VER% rmdir /S /Q  %KOPIA%
IF NOT EXIST %KOPIA% mkdir %KOPIA%
echo %VER% > %KOPIA%\%VER%
IF EXIST %KOPIA% GOTO KOPIA_OK
echo brak folderu docelowego, niepoprawna sciezka lub brak uprawnien
GOTO BLAD
:KOPIA_OK

IF NOT EXIST %LOG% mkdir %LOG%
IF EXIST %LOG% goto LOG_OK
echo brak/nie mozna utworzyc katalogu LOG
goto BLAD
:LOG_OK

IF EXIST %ROBO% GOTO ROBO_OK
echo niepoprawna sciezka do robocopy.exe
GOTO BLAD
:ROBO_OK


ver | find "XP" > NUL
if %ERRORLEVEL% == 0 goto XP
goto W7
:XP
SET OS="XP"
goto OS_OK
:W7
set OS="7"
goto OS_OK
:OS_OK

echo --------------------- %0 >> %LOGFILE%
echo OS %OS% >> %LOGFILE%
echo LOG %LOG% >> %LOGFILE%
echo LOGFILE %LOGFILE% >> %LOGFILE%
echo LOGROBO %LOGROBO% >> %LOGFILE%
rem echo LOGSRV %LOGSRV% >> %LOGFILE%
echo ROBO %ROBO% >> %LOGFILE%
echo PARMS %PARMS% >> %LOGFILE%
echo KOPIA %KOPIA% >> %LOGFILE%

rem ###########################################################################
call komunikat.bat

echo.
echo czyszcze:
echo |set /p="Foldery tymczasowe "
call czysc_temp.bat %KOPIA% %KIERUNEK% >> %LOGFILE% 2>nul
ping 127.0.0.1 -n %CZEKAJ% -w 1000 > NUL
echo wyczyszczono

echo.
echo kopiuje:

echo |set /p="Pulpit i Moje Dokumenty "
call kopia_pliki.bat %KOPIA% %KIERUNEK% >> %LOGFILE%
ping 127.0.0.1 -n %CZEKAJ% -w 1000 > NUL
echo skopiowano

rem echo |set /p="Mala ksiegowosc JM STUDIO "
rem call kopia_malaKS_jm.bat %KOPIA% %KIERUNEK% >> %LOGFILE%
rem rem ping 127.0.0.1 -n 3 -w 1000 > NUL
rem echo skopiowano

echo |set /p="Microsoft Outlook "
call kopia_outlook.bat %KOPIA% %KIERUNEK% >> %LOGFILE%
ping 127.0.0.1 -n %CZEKAJ% -w 1000 > NUL
echo skopiowano

echo |set /p="Microsoft Outlook Express "
call kopia_oexpress.bat %KOPIA% %KIERUNEK% >> %LOGFILE%
ping 127.0.0.1 -n %CZEKAJ% -w 1000 > NUL
echo skopiowano

echo |set /p="Mozilla Thunderbird "
call kopia_thunderbird.bat %KOPIA% %KIERUNEK% >> %LOGFILE%
ping 127.0.0.1 -n %CZEKAJ% -w 1000 > NUL
echo skopiowano

echo |set /p="Internet Explorer Zakladki "
call kopia_ulubioneie.bat %KOPIA% %KIERUNEK% >> %LOGFILE%
ping 127.0.0.1 -n %CZEKAJ% -w 1000 > NUL
echo skopiowano

echo |set /p="Mozilla Firefox "
call kopia_mozilla.bat %KOPIA% %KIERUNEK% >> %LOGFILE%
ping 127.0.0.1 -n %CZEKAJ% -w 1000 > NUL
echo skopiowano

echo |set /p="Opera "
call kopia_opera.bat %KOPIA% %KIERUNEK% >> %LOGFILE%
ping 127.0.0.1 -n %CZEKAJ% -w 1000 > NUL
echo skopiowano

echo |set /p="Skype "
call kopia_skype.bat %KOPIA% %KIERUNEK% >> %LOGFILE%
ping 127.0.0.1 -n %CZEKAJ% -w 1000 > NUL
echo skopiowano

echo |set /p="Soneta Enova - ustawienia lokalne "
call kopia_soneta.bat %KOPIA% %KIERUNEK% >> %LOGFILE%
ping 127.0.0.1 -n %CZEKAJ% -w 1000 > NUL
echo skopiowano

echo |set /p="Druki Gofin "
call kopia_gofin.bat %KOPIA% %KIERUNEK% >> %LOGFILE%
ping 127.0.0.1 -n %CZEKAJ% -w 1000 > NUL
echo skopiowano

echo |set /p="Kalkulator DT-1 "
call kopia_dt1.bat %KOPIA% %KIERUNEK% >> %LOGFILE%
ping 127.0.0.1 -n %CZEKAJ% -w 1000 > NUL
echo skopiowano

rem echo |set /p="Microsoft Office (test)"
rem call kopia_office.bat %KOPIA% %KIERUNEK% >> %LOGFILE%
rem ping 127.0.0.1 -n %CZEKAJ% -w 1000 > NUL
rem echo skopiowano

echo %DATE% %TIME% >> %LOGFILE%
echo %DATE% %TIME% > %LATEST%
echo %DATE% %TIME% %COMPUTERNAME% %USERNAME% %KOPIA% %KIERUNEK% >> %LOGLOG%
rem copy %LOGFILE% %LOGSRV% > NUL
rem copy %LOGROBO% %LOGSRV% > NUL
GOTO KONIEC

rem ############### BLAD ######################################################
:BLAD
echo nie utworzono kopii !!!
echo
echo Prosze o kontakt !!!
echo     Informatyk
pause

rem ############### KONIEC ####################################################
:KONIEC
