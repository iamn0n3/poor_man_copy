@echo off
rem
rem by Adrian Kubok
rem
rem 20140730 - utworzenie i poprawki
rem 20140725 -//-
rem
echo ------------------- %0
IF %KIERUNEK%==KOPIA goto KOPIA
IF %KIERUNEK%==PRZYWROC goto PRZYWROC
goto :BRAK

:KOPIA
SET SRC_SK="%APPDATA%\Skype"
SET DST_SK="%KOPIA%\skype"
goto KOPIUJ

:PRZYWROC
SET SRC_SK="%KOPIA%\skype"
SET DST_SK="%APPDATA%\Skype"
goto KOPIUJ

:KOPIUJ
echo %0 %2 %1
IF EXIST %SRC_SK%       %ROBO%      %SRC_SK%    %DST_SK%    %PARMS%
goto KONIEC

:BRAK
echo %0 : brak poprawnego parametru otrzymano %1 %2
goto KONIEC

:KONIEC

