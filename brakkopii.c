//+---------------------------------------------------------------------------
//
//  HELLO_WIN.C - Windows GUI 'Hello World!' Example
//
//+---------------------------------------------------------------------------

#include <windows.h>

#define APPNAME "Przypominam o kopii zapasowej"

char szAppName[] = APPNAME; // The name of this application
char szTitle[]   = APPNAME; // The title bar text
const char *pWindowText1;

void CenterWindow(HWND hWnd);
long GetTimeDifference(const PSYSTEMTIME pst1, const PSYSTEMTIME pst2);

//+---------------------------------------------------------------------------
//
//  Function:   WndProc
//
//  Synopsis:   very unusual type of function - gets called by system to
//              process windows messages.
//
//  Arguments:  same as always.
//----------------------------------------------------------------------------

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{


    switch (message) {

        // ----------------------- first and last
        case WM_CREATE:
            CenterWindow(hwnd);
            break;

        case WM_DESTROY:
            PostQuitMessage(0);
            break;

        // ----------------------- get out of it...
//        case WM_RBUTTONUP:
//            DestroyWindow(hwnd);
//            break;

//        case WM_KEYDOWN:
//            if (VK_ESCAPE == wParam)
//                DestroyWindow(hwnd);
//            break;

        // ----------------------- display our minimal info
        case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC         hdc;
            RECT        rc;
            hdc = BeginPaint(hwnd, &ps);

            GetClientRect(hwnd, &rc);
            SetTextColor(hdc, RGB(240,96,96));
            SetBkMode(hdc, TRANSPARENT);
            DrawText(hdc, pWindowText1, -1, &rc, DT_CENTER|DT_VCENTER);

            EndPaint(hwnd, &ps);
            break;
        }

        // ----------------------- let windows do all other stuff
        default:
            return DefWindowProc(hwnd, message, wParam, lParam);
    }
    return 0;
}

//+---------------------------------------------------------------------------
//
//  Function:   WinMain
//
//  Synopsis:   standard entrypoint for GUI Win32 apps
//
//----------------------------------------------------------------------------
int APIENTRY WinMain(
        HINSTANCE hInstance,
        HINSTANCE hPrevInstance,
        LPSTR lpCmdLine,
        int nCmdShow
        )
{
    MSG msg;
    WNDCLASS wc;
    HWND hwnd;

    pWindowText1 = "\n\n\n\n\n\nPrzez ostatnie 2 tygodnie\nNIE wykonano kopii zapasowej\nprosze wykonac kopie zapasowa!";

    HANDLE hFILE;
    TCHAR szBuf[MAX_PATH];
    FILETIME ftCreate, ftAccess, ftWrite;
    SYSTEMTIME stUTC, stLocal, stAktualny;
    DWORD dwRet;

    hFILE=CreateFile("U:\\autokopia\\kopia.001",GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,0,NULL);
    if(hFILE!=INVALID_HANDLE_VALUE){
        GetFileTime(hFILE,&ftCreate,&ftAccess,&ftWrite);
        FileTimeToSystemTime(&ftWrite, &stUTC);
        SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal);
        GetLocalTime(&stAktualny);
//        printf("data: %04d-%02d-%02d %02d-%02d\n",stLocal.wYear,stLocal.wMonth,stLocal.wDay,stLocal.wHour,stLocal.wMinute);
//        printf("data: %04d-%02d-%02d %02d-%02d\n",stAktualny.wYear,stAktualny.wMonth,stAktualny.wDay,stAktualny.wHour,stAktualny.wMinute);
//        printf("\n%d\n\n",GetTimeDifference(&stLocal,&stAktualny));
        if(GetTimeDifference(&stLocal,&stAktualny)<=14){
            return 0;
        }
    }


    // Fill in window class structure with parameters that describe
    // the main window.

    ZeroMemory(&wc, sizeof wc);
    wc.hInstance     = hInstance;
    wc.lpszClassName = szAppName;
    wc.lpfnWndProc   = (WNDPROC)WndProc;
    wc.style         = CS_DBLCLKS|CS_VREDRAW|CS_HREDRAW;
    wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
    wc.hCursor       = LoadCursor(NULL, IDC_ARROW);

    if (FALSE == RegisterClass(&wc))
        return 0;

    // create the browser
    hwnd = CreateWindow(
        szAppName,
        szTitle,
        WS_CAPTION|WS_VISIBLE|WS_SYSMENU,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        800,//CW_USEDEFAULT,
        600,//CW_USEDEFAULT,
        0,
        0,
        hInstance,
        0);

    if (NULL == hwnd)
        return 0;

    // Main message loop:
    while (GetMessage(&msg, NULL, 0, 0) > 0) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return msg.wParam;
}

//+---------------------------------------------------------------------------

//+---------------------------------------------------------------------------

void CenterWindow(HWND hwnd_self)
{
    HWND hwnd_parent;
    RECT rw_self, rc_parent, rw_parent;
    int xpos, ypos;

    hwnd_parent = GetParent(hwnd_self);
    if (NULL == hwnd_parent)
        hwnd_parent = GetDesktopWindow();

    GetWindowRect(hwnd_parent, &rw_parent);
    GetClientRect(hwnd_parent, &rc_parent);
    GetWindowRect(hwnd_self, &rw_self);

    xpos = rw_parent.left + (rc_parent.right + rw_self.left - rw_self.right) / 2;
    ypos = rw_parent.top + (rc_parent.bottom + rw_self.top - rw_self.bottom) / 2;

    SetWindowPos(
        hwnd_self, NULL,
        xpos, ypos, 0, 0,
        SWP_NOSIZE|SWP_NOZORDER|SWP_NOACTIVATE
        );
}

//+---------------------------------------------------------------------------
long GetTimeDifference(const PSYSTEMTIME pst1, const PSYSTEMTIME pst2)
{
	static const ULONGLONG FT_SECOND = ((ULONGLONG) 10000000);
	static const ULONGLONG FT_MINUTE = (60 * ((ULONGLONG) 10000000));
	static const ULONGLONG FT_HOUR   = (60 * (60 * ((ULONGLONG) 10000000)));
	static const ULONGLONG FT_DAY    = (24 * (60 * (60 * ((ULONGLONG) 10000000))));

	// Convert each SYSTEMTIME structure into a FILETIME one...
	FILETIME ft1, ft2;
	SystemTimeToFileTime(pst1, &ft1);
	SystemTimeToFileTime(pst2, &ft2);

	// Convert to large integers...
	ULARGE_INTEGER li1 = {0};
	memcpy(&li1, &ft1, sizeof(FILETIME));

	ULARGE_INTEGER li2 = {0};
	memcpy(&li2, &ft2, sizeof(FILETIME));

	// Subtract the two dates...
	ULONGLONG ullNanoDiff = li2.QuadPart - li1.QuadPart;

	// FILETIME's measure the number of 100-nanosecond intervals.  Convert this to seconds...
	long lSecDiff = (long) (ullNanoDiff / FT_SECOND);

	// Finally, convert seconds to minutes and hours, if necessary...
    return (lSecDiff/60/60/24);
	//*piHours = (int) lSecDiff / 3600;
	//lSecDiff = lSecDiff % 3600;
	//*piMins  = lSecDiff / 60;
	//*piSecs  = lSecDiff % 60;
}