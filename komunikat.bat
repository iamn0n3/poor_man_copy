@echo off

rem
rem by Adrian Kubok
rem
rem 20140730 - utworzono
rem

:PETLA
set OK=0

set OUTLOOK=0
set OEXPRESS=0
set THUNDERBIRD=0
set FIREFOX=0
set OPERA=0
set SKYPE=0
set SONETA=0

call czy_uruchomiony.bat outlook.exe
IF %ERRORLEVEL%==0 set OUTLOOK=1

call czy_uruchomiony.bat msimn.exe
IF %ERRORLEVEL%==0 set OEXPRESS=1

call czy_uruchomiony.bat thunderbird.exe
IF %ERRORLEVEL%==0 set THUNDERBIRD=1

call czy_uruchomiony.bat firefox.exe
IF %ERRORLEVEL%==0 set FIREFOX=1

call czy_uruchomiony.bat opera.exe
IF %ERRORLEVEL%==0 set OPERA=1

rem call czy_uruchomiony.bat Skype.exe
rem IF %ERRORLEVEL%==0 set SKYPE=1

call czy_uruchomiony.bat SonetaExplorer.exe
IF %ERRORLEVEL%==0 set SONETA=1

IF %KIERUNEK%==KOPIA color 0A
IF %KIERUNEK%==PRZYWROC color 04

cls
echo.
echo              ##   #  # ###  ##  #  #  ##  ###  #  ##
echo             #  #  #  #  #  #  # # #  #  # #  # # #  #
echo             ####  #  #  #  #  # ###  #  # ###  # ####
echo             #  #   ##   #   ##  #  #  ##  #    # #  #
echo            ===========================================
echo.
echo Prosze o zapisanie i zakmniecie wszystkich plikow z Pulpitu
echo         oraz folderu "Moje Domumenty"
echo.
echo Otwarte dokuemnty nie beda skopiowane
echo.
echo Po zamknieciu nastepujaych programow kopia zostanie automatycznie kontynuowana
IF %OUTLOOK%==1 echo  - Microsoft Outlook
IF %OEXPRESS%==1 echo  - Microsoft Outlook Express
IF %THUNDERBIRD%==1 echo  - Mozilla Thunderbird
IF %FIREFOX%==1 echo  - Mozilla Firefox
IF %OPERA%==1 echo  - Opera
IF %SKYPE%==1 echo  - Skype
IF %SONETA%==1 echo  - Soneta Enova
echo.
echo.




rem czekaj
ping 127.0.0.1 -n 5 -w 1000 > NUL

SET /A OK=%OUTLOOK%+%OEXPRESS%+%THUNDERBIRD%+%FIREFOX%+%OPERA%+%SKYPE%+%SONETA%


IF %OK%==0 goto KONIEC
goto PETLA

:KONIEC
cls
echo.
echo              ##   #  # ###  ##  #  #  ##  ###  #  ##
echo             #  #  #  #  #  #  # # #  #  # #  # # #  #
echo             ####  #  #  #  #  # ###  #  # ###  # ####
echo             #  #   ##   #   ##  #  #  ##  #    # #  #
echo            ===========================================
echo.
echo.
echo Tworzenie kopii zapasowej zostalo rozpoczete.
echo Okno zostanie zamkniete gdy kopia zostanie zakonczona
echo.
echo.

