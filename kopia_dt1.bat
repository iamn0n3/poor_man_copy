@echo off
rem
rem by Adrian Kubok
rem
rem 20140730 - utworzenie i poprawki
rem 20140725 -//-
rem
echo ------------------- %0
IF %KIERUNEK%==KOPIA goto KOPIA
IF %KIERUNEK%==PRZYWROC goto PRZYWROC

rem tylko u Pana Czeslawa na windows 7
IF %OS%=="XP" goto KONIEC
goto :BRAK

:KOPIA
SET SRC="%HOMEDRIVE%\Program Files (x86)\podatki.biz"
SET DST="%KOPIA%\dt1"
goto KOPIUJ

:PRZYWROC
SET SRC="%KOPIA%\dt1"
SET DST="%HOMEDRIVE%\Program Files (x86)\podatki.biz"
goto KOPIUJ

:KOPIUJ
echo %0 %2 %1
IF EXIST %SRC%       %ROBO%      %SRC%    %DST%    %PARMS%

goto KONIEC

:BRAK
echo %0 : brak poprawnego parametru
echo otrzymano %1 %2
goto KONIEC

:KONIEC

