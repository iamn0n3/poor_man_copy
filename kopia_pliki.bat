@echo off
rem
rem by Adrian Kubok
rem
rem 20140730 - utworzenie i poprawki
rem 20140725 -//-
rem
echo ------------------- %0
IF %KIERUNEK%==KOPIA goto KOPIA
IF %KIERUNEK%==PRZYWROC goto PRZYWROC
goto :BRAK

:KOPIA
IF %OS%=="XP" SET SRC_PULPIT="%USERPROFILE%\Pulpit"
IF %OS%=="7"  SET SRC_PULPIT="%USERPROFILE%\Desktop"
SET DST_PULPIT="%KOPIA%\pulpit"

IF %OS%=="XP" SET SRC_DOC="%USERPROFILE%\Moje dokumenty"
IF %OS%=="7"  SET SRC_DOC="%USERPROFILE%\Documents"
SET DST_DOC="%KOPIA%\dokumenty"
goto KOPIUJ

:PRZYWROC
SET SRC_PULPIT="%KOPIA%\pulpit"
IF %OS%=="XP" SET DST_PULPIT="%USERPROFILE%\Pulpit"
IF %OS%=="7"  SET DST_PULPIT="%USERPROFILE%\Desktop"

SET SRC_DOC="%KOPIA%\dokumenty"
IF %OS%=="XP" SET DST_DOC="%USERPROFILE%\Moje dokumenty"
IF %OS%=="7"  SET DST_DOC="%USERPROFILE%\Documents"
goto KOPIUJ

:KOPIUJ
echo %0 %1 %2
IF EXIST %SRC_PULPIT%   %ROBO%      %SRC_PULPIT%    %DST_PULPIT%    %PARMS%
IF EXIST %SRC_DOC%      %ROBO%      %SRC_DOC%       %DST_DOC%       %PARMS%
goto KONIEC

:BRAK
echo %0 : brak poprawnego parametru otrzymano: %1 %2
goto KONIEC

:KONIEC

