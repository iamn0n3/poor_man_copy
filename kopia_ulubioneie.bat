@echo off
rem
rem by Adrian Kubok
rem
rem 20140730 - utworzenie i poprawki
rem 20140725 -//-
rem
echo ------------------- %0
IF %KIERUNEK%==KOPIA goto KOPIA
IF %KIERUNEK%==PRZYWROC goto PRZYWROC
goto :BRAK

:KOPIA
IF %OS%=="XP" SET SRC_IE="%USERPROFILE%\Ulubione"
IF %OS%=="7"  SET SRC_IE="%USERPROFILE%\Favorites"
SET DST_IE="%KOPIA%\ie\ulubione"
goto KOPIUJ

:PRZYWROC
SET SRC_IE="%KOPIA%\ie\ulubione"
IF %OS%=="XP" SET DST_IE="%USERPROFILE%\Ulubione"
IF %OS%=="7"  SET DST_IE="%USERPROFILE%\Favorites"
goto KOPIUJ

:KOPIUJ
echo %0 %2 %1
IF NOT EXIST %DST_IE% mkdir %DST_IE%
IF EXIST %SRC_IE%       %ROBO%      %SRC_IE%    %DST_IE%    %PARMS%
goto KONIEC

:BRAK
echo %0 : brak poprawnego parametru otrzymano %1 %2
goto KONIEC

:KONIEC

