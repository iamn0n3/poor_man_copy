@echo off
rem
rem by Adrian Kubok
rem
rem 20160223 - utworzono
rem 20160224 - poprawki, dodano kolejne czyszczenia
rem
echo ------------------- %0
IF %KIERUNEK%==KOPIA goto KOPIA
IF %KIERUNEK%==PRZYWROC goto PRZYWROC
goto :BRAK

:KOPIA
goto KOPIUJ

:PRZYWROC
rem nic nie robi przy przywracaniu kopii
goto KONIEC

:KOPIUJ
echo %0 %2 %1
rem czysc folder TEMP
del "%TEMP%\*.*" /f /s /q
for /D %%i in ("%TEMP%\*") do RD /S /Q "%%i"

rem czysc folder TMP
del "%TMP%\*.*" /f /s /q
for /D %%i in ("%TMP%\*") do RD /S /Q "%%i"

rem czysc ostatnio otwarte
if exist "%userprofile%\Recent" del /f /s /q "%userprofile%\Recent"
if exist "%userprofile%\Recent" for /D %%i in ("%userprofile%\Recent") do RD /S /Q "%%i"

rem Clear Google Chrome cache
del "%LOCALAPPDATA%\Google\Chrome\User Data\*.*" /f /s /q
for /D %%i in ("%LOCALAPPDATA%\Google\Chrome\User Data\*") do RD /S /Q "%%i"

rem Clear Firefox cache
del "%LOCALAPPDATA%\Mozilla\Firefox\Profiles\*.*" /f /s /q
for /D %%i in ("%LOCALAPPDATA%\Mozilla\Firefox\Profiles\*") do RD /S /Q "%%i"%

rem czysc soneta
RD /S /Q "%APPDATA%\Soneta\Cache"

rem czysc APPDATA
rem cscript CleanRoamingProfile.vbs //nologo

goto KONIEC

:BRAK
echo %0 : brak poprawnego parametru otrzymano %1 %2
goto KONIEC

:KONIEC

