@echo off
rem
rem by Adrian Kubok
rem
rem 20140730 - utworzenie i poprawki
rem 20140725 -//-
rem
echo ------------------- %0
IF %KIERUNEK%==KOPIA goto KOPIA
IF %KIERUNEK%==PRZYWROC goto PRZYWROC
goto :BRAK

:KOPIA
SET SRC_SO="%APPDATA%\Soneta"
SET DST_SO="%KOPIA%\soneta"
goto KOPIUJ

:PRZYWROC
SET SRC_SO="%KOPIA%\soneta"
SET DST_SO="%APPDATA%\Soneta"
goto KOPIUJ

:KOPIUJ
echo %0 %2 %1
IF EXIST %SRC_SO%       %ROBO%      %SRC_SO%    %DST_SO%    %PARMS%
goto KONIEC

:BRAK
echo %0 : brak poprawnego parametru otrzymano %1 %2
goto KONIEC

:KONIEC

