@echo off
rem
rem by Adrian Kubok
rem
rem 20140730 - utworzono
rem
rem kody wyjsciowe:
rem 0 - program jest uruchomiony
rem 1 - program jest wylaczony
rem 2 - nie podano parametru
rem

rem zobacz jaki system
rem domyslnie przyjmij 7

set OBRAZ=IMAGENAME
ver | find "XP" > nul
IF %ERRORLEVEL% == 0 set OBRAZ=NAZWA_OBRAZU

IF "%1"=="" exit /b 2
tasklist /FI "%OBRAZ% eq %1" 2>&1 | find /I "%1" > NUL 2>&1
rem echo errorlevel %ERRORLEVEL%
IF %ERRORLEVEL%==0 exit /b 0
exit /b 1

