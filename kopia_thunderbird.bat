@echo off
rem
rem by Adrian Kubok
rem
rem 20140730 - utworzenie i poprawki
rem 20140725 -//-
rem
echo ------------------- %0
IF %KIERUNEK%==KOPIA goto KOPIA
IF %KIERUNEK%==PRZYWROC goto PRZYWROC
goto :BRAK

:KOPIA
rem thunderbird standardowy
SET SRC_TB="%APPDATA%\Thunderbird"
SET DST_TB="%KOPIA%\thunderbird"
rem thunderbird u Szefa
SET SRC_SZ="%SystemDrive%\POCZTA_ADAM"
SET DST_SZ="%KOPIA%\thunderbird-Szef"
goto KOPIUJ

:PRZYWROC
rem thunderbird standardowy
SET SRC_TB="%KOPIA%\thunderbird"
SET DST_TB="%APPDATA%\Thunderbird"
rem thunderbird u Szefa
SET SRC_SZ="%KOPIA%\thunderbird-Szef"
SET DST_SZ="%SystemDrive%\POCZTA_ADAM"
IF NOT EXIST %DST_TB% mkdir %DST_TB%
IF EXIST %SRC_SZ% IF NOT EXIST %DST_SZ% mkdir %DST_SZ%
goto KOPIUJ

:KOPIUJ
echo %0 %2 %1
IF EXIST %SRC_TB%      %ROBO%      %SRC_TB%    %DST_TB%    %PARMS%
IF EXIST %SRC_SZ%      %ROBO%      %SRC_SZ%    %DST_SZ%    %PARMS%
goto KONIEC

:BRAK
echo %0 : brak poprawnego parametru otrzymano %1 %2
goto KONIEC

:KONIEC

