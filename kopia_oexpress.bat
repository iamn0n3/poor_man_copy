@echo off
rem
rem by Adrian Kubok
rem
rem 20140730 - utworzenie i poprawki
rem 20140725 -//-
rem
echo ------------------- %0
IF %KIERUNEK%==KOPIA goto KOPIA
IF %KIERUNEK%==PRZYWROC goto PRZYWROC
goto :BRAK

:KOPIA
echo %0 %2 %1
rem outlook express (tylko w xp, niema migracji niema potrzeby sprawdzania)
SET SRC_OE="%USERPROFILE%\Ustawienia lokalne\Dane aplikacji\Identities"
SET SRC_OE2="%APPDATA%\Microsoft\Address Book"
SET DST_OE="%KOPIA%\outlook_express\poczta"
SET DST_OE2="%KOPIA%\outlook_express\kontakty"
goto KOPIUJ

:PRZYWROC
SET SRC_OE="%KOPIA%\outlook_express\poczta"
SET SRC_OE2="%KOPIA%\outlook_express\kontakty"
SET DST_OE="%USERPROFILE%\Ustawienia lokalne\Dane aplikacji\Identities"
SET DST_OE2="%APPDATA%\Microsoft\Address Book"
IF NOT EXIST %DST_OE% mkdir %DST_OE%
IF NOT EXIST %DST_OE2% mkdir %DST_OE2%
goto KOPIUJ

:KOPIUJ
IF EXIST %SRC_OE%       %ROBO%      %SRC_OE%        %DST_OE%        %PARMS%
IF EXIST %SRC_OE2%      %ROBO%      %SRC_OE2%       %DST_OE2%       %PARMS%
goto KONIEC

:BRAK
echo %0 : brak poprawnego parametru otrzymano %1 %2
goto KONIEC

:KONIEC

