@echo off
rem
rem by Adrian Kubok
rem
rem 20140730 - utworzenie i poprawki
rem 20140725 -//-
rem
echo ------------------- %0
IF %KIERUNEK%==KOPIA goto KOPIA
IF %KIERUNEK%==PRZYWROC goto PRZYWROC
goto :BRAK

:KOPIA
SET SRC_OP="%APPDATA%\Opera"
SET DST_OP="%KOPIA%\opera"
goto KOPIUJ

:PRZYWROC
SET SRC_OP="%KOPIA%\opera"
SET DST_OP="%APPDATA%\Opera"
goto KOPIUJ

:KOPIUJ
echo %0 %2 %1
IF EXIST %SRC_OP%       %ROBO%      %SRC_OP%    %DST_OP%    %PARMS%
goto KONIEC

:BRAK
echo %0 : brak poprawnego parametru otrzymano %1 %2
goto KONIEC

:KONIEC

