@echo off
rem
rem by Adrian Kubok
rem
rem 20140730 - utworzenie i poprawki
rem 20140725 -//-
rem
echo ------------------- %0
IF %KIERUNEK%==KOPIA goto KOPIA
IF %KIERUNEK%==PRZYWROC goto PRZYWROC
goto :BRAK

:KOPIA
rem pulpit
IF %OS%=="XP" SET SRC_OUL="%USERPROFILE%\Ustawienia lokalne\Dane aplikacji\Microsoft\Outlook"
IF %OS%=="7"  SET SRC_OUL="%LOCALAPPDATA%\Microsoft\Outlook"
SET DST_OUL="%KOPIA%\outlook\local"
SET SRC_OUR="%APPDATA%\Microsoft\Outlook"
SET DST_OUR="%KOPIA%\outlook\roaming"
SET REG_OU="%KOPIA%\outlook\profil.reg"
IF NOT EXIST u:\autokopia\outlook mkdir u:\autokopia\outlook
IF EXIST %REG_OU% del %REG_OU%
reg export "HKEY_CURRENT_USER\Software\Microsoft\Windows NT\CurrentVersion\Windows Messaging Subsystem\Profiles" %REG_OU% /y 2>NUL
goto KOPIUJ

:PRZYWROC
rem pulpit
SET SRC_OUL="%KOPIA%\outlook\local"
SET SRC_OUR="%KOPIA%\outlook\roaming"
SET DST_OUR="%APPDATA%\Microsoft\Outlook"
SET REG_OU="%KOPIA%\outlook\profil.reg"
IF %OS%=="XP" SET DST_OUL="%USERPROFILE%\Ustawienia lokalne\Dane aplikacji\Microsoft\Outlook"
IF %OS%=="7"  SET DST_OUL="%LOCALAPPDATA%\Microsoft\Outlook"
rem ustawiac recznie - jeste problem z wtyczkami i ustawieniami potem
rem reg import %REG_OU% > NUL
goto KOPIUJ

:KOPIUJ
echo %0 %2 %1
IF NOT EXIST %DST_OUL% mkdir %DST_OUL%
IF NOT EXIST %DST_OUR% mkdir %DST_OUR%
IF EXIST %SRC_OUL%       %ROBO%      %SRC_OUL%    %DST_OUL%    %PARMS%
IF EXIST %SRC_OUR%       %ROBO%      %SRC_OUR%    %DST_OUR%    %PARMS%
goto KONIEC

:BRAK
echo %0 : brak poprawnego parametru otrzymano %1 %2
goto KONIEC

:KONIEC

