@echo off
rem
rem by Adrian Kubok
rem
rem 20160110 - utworzenie i poprawki
rem
echo ------------------- %0
IF %KIERUNEK%==KOPIA goto KOPIA
IF %KIERUNEK%==PRZYWROC goto PRZYWROC
goto :BRAK

:KOPIA
IF %OS%=="XP" SET SRC_OFF="%APPDATA%\Roaming\Microsoft"
IF %OS%=="7"  SET SRC_OFF="%APPDATA%\Microsoft"
SET DST_OFF="%KOPIA%\office\microsoft_roaming"
SET REG_OFF="%KOPIA%\office\office.reg"
SET REG_OFF2="%KOPIA%\office\office_wow.reg"
IF NOT EXIST u:\autokopia\office mkdir u:\autokopia\office\microsoft_roaming
IF EXIST %REG_OFF% del %REG_OFF%
reg /Y export "HKEY_CURRENT_USER\Software\Microsoft\Office" %REG_OFF%
reg /Y export "HKEY_CURRENT_USER\SOFTWARE\Wow6432Node\Microsoft" %REG_OFF2%
goto KOPIUJ

:PRZYWROC
SET SRC_OFF="%KOPIA%\office\microsoft_roaming"
SET REG_OFF="%KOPIA%\office\office.reg"
IF %OS%=="XP" SET DST_OUL="%APPDATA%\Roaming\Microsoft"
IF %OS%=="7"  SET DST_OUL="%APPDATA%\Microsoft"
rem dodac recznie - jeste problem z wtyczkami i ustawieniami potem
rem reg import %REG_OFF% > NUL
rem reg import %REG_OFF2% > NUL
goto KOPIUJ

:KOPIUJ
echo %0 %2 %1
IF NOT EXIST %DST_OFF% mkdir %DST_OFF%
IF EXIST %SRC_OFF%       %ROBO%      %SRC_OFF%    %DST_OFF%    %PARMS%
goto KONIEC

:BRAK
echo %0 : brak poprawnego parametru otrzymano %1 %2
goto KONIEC

:KONIEC

