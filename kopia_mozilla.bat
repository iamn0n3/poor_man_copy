@echo off
rem
rem by Adrian Kubok
rem
rem 20140730 - utworzenie i poprawki
rem 20140725 -//-
rem
echo ------------------- %0
IF %KIERUNEK%==KOPIA goto KOPIA
IF %KIERUNEK%==PRZYWROC goto PRZYWROC
goto :BRAK

:KOPIA
SET SRC_MZ="%APPDATA%\Mozilla"
SET DST_MZ="%KOPIA%\mozilla"
goto KOPIUJ

:PRZYWROC
SET SRC_MZ="%KOPIA%\mozilla"
SET DST_MZ="%APPDATA%\Mozilla"
goto KOPIUJ

:KOPIUJ
echo %0 %2 %1
IF NOT EXIST %DST_MZ% mkdir %DST_MZ%
IF EXIST %SRC_MZ%       %ROBO%      %SRC_MZ%    %DST_MZ%    %PARMS%
goto KONIEC

:BRAK
echo %0 : brak poprawnego parametru otrzymano %1 %2
goto KONIEC

:KONIEC

