@echo off
rem
rem by Adrian Kubok
rem
rem 20140730 - utworzenie i poprawki
rem 20140725 -//-
rem
echo ------------------- %0
IF %KIERUNEK%==KOPIA goto KOPIA
IF %KIERUNEK%==PRZYWROC goto PRZYWROC
goto :BRAK

:KOPIA
SET SRC_GOFIN="%APPDATA%\GofinDruki"
SET DST_GOFIN="%KOPIA%\gofin"
goto KOPIUJ

:PRZYWROC
SET SRC_GOFIN="%KOPIA%\gofin"
SET DST_GOFIN="%APPDATA%\GofinDruki"
goto KOPIUJ

:KOPIUJ
echo %0 %2 %1
IF EXIST %SRC_GOFIN%       %ROBO%      %SRC_GOFIN%    %DST_GOFIN%    %PARMS%
goto KONIEC

:BRAK
echo %0 : brak poprawnego parametru otrzymano %1 %2
goto KONIEC

:KONIEC

